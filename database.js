require("dotenv").config();
const mongoose = require("mongoose");

/**
 * Abre la conexión a la base de datos
 */
function dbConnect() {
  // Abre la conexión a la base de datos
  mongoose.connect(process.env.DB_CONNECTION, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  });
  
  // Referencia a la base de datos
  const db = mongoose.connection;
  // Verifica si hay error de conexión
  db.on("error", console.error.bind(console, "connection error:"));
  // Verifica la conexión abierta
  db.once("open", function () {
    console.log(`Conectado a la base de datos...`);
  });
}

module.exports = dbConnect;