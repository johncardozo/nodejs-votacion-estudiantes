const { Router } = require("express");
const router = Router();
const { isAuthenticated } = require("../helpers/auth");
const Estudiante = require("../models/estudiante");

router.get("/", isAuthenticated, async (req, res) => {
  // Obtiene el usuario de la sesión
  const idUsuario = req.user.id;

  let usuario;
  let candidatos;
  try {
    // Busca la información del estudiante autenticado
    usuario = await Estudiante.findById(idUsuario);

    // Obtiene el semestre y la facultad del estudiante autenticado
    const semestre = usuario.semestre;
    const idFacultad = usuario.facultad.id;

    // Busca los estudiantes del mismo semestre, de la misma facultad que sean candidatos
    candidatos = await Estudiante.find({
      semestre: semestre,
      esCandidato: true,
      "facultad.id": idFacultad,
    });
  } catch (error) {
    console.log(error);
  }
  // Datos para el view
  const datos = {
    usuario: usuario,
    candidatos: candidatos,
  };
  // Muestra la página con los datos
  res.render("index", datos);
});

router.post("/", isAuthenticated, async (req, res) => {
  // Obtiene el usuario de la sesión
  const idUsuario = req.user.id;

  // Veririca si el estudiante autenticado ya votó
  const votante = Estudiante.findById({ idUsuario });
  if (votante.yaVoto) {
    res.redirect("/");
  }

  // Obtiene el candidato seleccionado
  const idCandidato = req.body.candidato;

  // Verifica si se seleccionó un candidato
  if (idCandidato === undefined) {
    console.log("No se seleccionó un candidato");
  } else {
    // Busca la información del candidato por el cual se va a votar
    let candidato = await Estudiante.findById(idCandidato);

    // Verifica si el candidato tiene votos y le suma uno más
    let nuevosVotos = candidato.votos == undefined ? 1 : candidato.votos + 1;

    // Incrementa los votos del candidato
    await Estudiante.findOneAndUpdate(
      { _id: idCandidato },
      { votos: nuevosVotos }
    );

    // Actualiza el usuario votante asignando los datos del candidato por el que votó
    const res = await Estudiante.findOneAndUpdate(
      { _id: idUsuario },
      {
        $set: {
          candidato: {
            id: candidato._id,
            documento: candidato.documento,
            apellidos: candidato.apellidos,
            nombres: candidato.nombres,
          },
          yaVoto: true,
          fechaVotacion: new Date(),
        },
      }
    );
  }
  // Redirecciona a la página principal
  res.redirect("/");
});

module.exports = router;
