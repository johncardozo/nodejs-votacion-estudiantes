const { Router } = require("express");
const router = Router();

const Estudiante = require("../models/estudiante");
const { json } = require("body-parser");

router.get("/r/:id", async (req, res) => {
  let idFacultad = 0;
  // Convierte el parámetro a entero
  idFacultad = parseInt(req.params.id);

  // Si el valor no era un entero
  idFacultad = isNaN(idFacultad) ? 0 : idFacultad;

  // Obtiene la cantidad total de no votantes
  const totalNoVotantes = await Estudiante.countDocuments({
    "facultad.id": idFacultad,
    yaVoto: false,
  });

  // Obtiene la cantidad total de votantes
  const totalVotantes = await Estudiante.countDocuments({
    "facultad.id": idFacultad,
    yaVoto: true,
  });

  // Obtiene los candidatos ordenados por:
  // 1. Semestre: ascendente
  // 2. Votos: descendente
  const candidatos = await Estudiante.find({
    esCandidato: true,
    "facultad.id": idFacultad,
  }).sort({ semestre: 1, votos: -1 });

  // Obtiene la cantidad de estudiantes de la facultad por semestre
  const estudiantesPorSemestre = await Estudiante.aggregate([
    { $match: { "facultad.id": idFacultad } },
    {
      $group: {
        _id: "$semestre",
        cantidadEstudiantes: {
          $sum: 1,
        },
      },
    },
    { $sort: { _id: 1 } },
  ]);

  // Obtiene la cantidad de votantes por semestre
  const votantesPorSemestre = await Estudiante.aggregate([
    { $match: { "facultad.id": idFacultad, yaVoto: true } },
    {
      $group: {
        _id: "$semestre",
        cantidadVotantes: {
          $sum: 1,
        },
      },
    },
    { $sort: { _id: 1 } },
  ]);

  // Recorre la lista de estudiantes por semestre
  let listaSemestres = [];
  estudiantesPorSemestre.forEach((e) => {
    // Busca los votantes por semestre
    const encontrado = votantesPorSemestre.find((v) => v._id === e._id);
    let nuevo = {};
    // Crea el total de votantes por semestre
    nuevo["totalVotantes"] =
      encontrado === undefined ? 0 : encontrado.cantidadVotantes;
    // Crea el total de no votantes por semestre
    nuevo["totalNoVotantes"] = e.cantidadEstudiantes - nuevo["totalVotantes"];
    // Agrega el semestre a la lista
    listaSemestres.push(nuevo);
  });

  const contexto = {
    candidatos: candidatos,
    totalVotantes: totalVotantes,
    totalNoVotantes: totalNoVotantes,
    totalEstudiantes: totalVotantes + totalNoVotantes,
    listaSemestres: JSON.stringify(listaSemestres),
  };

  // Muestra la página
  res.render("report", contexto);
});

module.exports = router;

// // Cantidad de estudiantes por semestre
// // de una facultad específica ordenados por semestre
// db.estudiantes.aggregate(
//   [
//       {
//           "$match" : {
//               "facultad.id" : 1
//           }
//       },
//       {
//           "$group" : {
//               "_id" : "$semestre",
//               "cantidadEstudiantes" : {
//                   "$sum" : 1
//               }
//           }
//       },
//       {
//           "$sort" : {
//               "_id" : 1
//           }
//       }
//   ]
// )
// // Cantidad de votantes por semestre
// // de una facultad específica ordenados por semestre
// db.estudiantes.aggregate(
//   [
//       {
//           "$match" : {
//               "facultad.id" : 1, "yaVoto": true
//           }
//       },
//       {
//           "$group" : {
//               "_id" : "$semestre",
//               "cantidadVotantes" : {
//                   "$sum" : 1
//               }
//           }
//       },
//       {
//           "$sort" : {
//               "_id" : 1
//           }
//       }
//   ]
// )
