const { Router } = require("express");
const router = Router();
const passport = require("passport");

router.get("/login", (req, res) => {
  // Verifica si el usuario ya está autenticado
  if (req.isAuthenticated()) {
    // Redirecciona a l página principal
    res.redirect("/");
  } else {
    // Muestra la página de login
    res.render("login");
  }
});

router.post(
  "/login",
  passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true,
  })
);

router.get("/logout", (req, res) => {
  // Cierra sesión
  req.logOut();
  // Redirecciona al login
  res.redirect("/login");
});

module.exports = router;
