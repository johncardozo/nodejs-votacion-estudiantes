require("dotenv").config();
const express = require("express");
const app = express();
const path = require("path");
const flash = require("express-flash");
const session = require("express-session");
const passport = require("passport");
require("./config/passport-config");
const dbConnect = require("./database");

// Puerto de ejecución de la aplicación
const port = process.env.port || 3000;

// Establece el motor de views
app.set("view engine", "pug");

// Crea un path virtual al directorio de archivos estáticos
app.use("/static", express.static(path.join(__dirname, "static")));

// Middleware para poder leer el body de un post
app.use(
  express.urlencoded({
    extended: true,
  })
);

// Configuración de middlewares
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// Rutas de la aplicación
app.use(require('./routes/login.routes'));
app.use(require('./routes/index.routes'));
app.use(require('./routes/report.routes'));

// Conexión a la base de datos
dbConnect();

// Inicia la aplicación
app.listen(port, () =>
  console.log(`Aplicación funcionando por el puerto ${port}...`)
);
