#!/usr/bin/python3

import sys
import getopt
import pandas as pd
import json

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('generar-json.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('generar-json.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg

    # Lee el archivo CSV
    data_from_csv = pd.read_csv(inputfile, encoding='utf8')

    datos = []
    for i, j in data_from_csv.iterrows():
        # Crea el elemento
        elemento = {
            "codigo": j['codigo'],
            "apellidos": j['apellidos'],
            "nombres": j['nombres'],
            "documento": j['documento'],
            "semestre": j['semestre'],
            "email": j['email'],
            "votos": j['votos'],
            "yaVoto": j['yaVoto'],
            "facultad": {
                "id": j['facultad_id'],
                "nombre": j['facultad_nombre']
            },
            "esCandidato": j['esCandidato']
        }
        # Agrega el elemento
        datos.append(elemento)

    archivo_salida = open(outputfile, 'w')
    json.dump(datos, archivo_salida)
    archivo_salida.close()


if __name__ == "__main__":
    main(sys.argv[1:])
