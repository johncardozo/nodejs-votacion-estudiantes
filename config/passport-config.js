const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;

const Estudiante = require("../models/estudiante");

passport.use(
  new LocalStrategy(async (username, password, done) => {
    // Busca el usuario por email en la bd
    const user = await Estudiante.findOne({ email: username });

    // No hay un usuario con el email recibido
    if (!user) {
      return done(null, false, { message: "Email incorrecto" });
    } else {
      // El password es incorrecto
      if (user.documento !== password) {
        return done(null, false, { message: "Documento incorrecto" });
      } else {
        // FIXME: Esta validación es temporal. Se debe quitar
        // Si el estudiante es de Ing. Industrial
        if(user.facultad.id === 2){
          return done(null, false, { message: "Las votaciones para Ing. Industrial ya terminaron" });
        }

        return done(null, user);
      }
    }
  })
);

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  Estudiante.findById(id, (err, user) => {
    done(err, user);
  });
});
