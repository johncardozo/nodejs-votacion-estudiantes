require("dotenv").config();
const mongoose = require("mongoose");
// Conexion
mongoose.connect(process.env.DB_CONNECTION, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
// Referencia a la base de datos
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
  console.log("conectado!!!");
});
// Definicion del Schema
const kittySchema = new mongoose.Schema({
  name: String,
});
// Creacion del modelo basado en el Schema
const Kitten = mongoose.model("Kitten", kittySchema);
// Crea un objeto
const fluffy = new Kitten({ name: "Fanny" });
// Guarda el modelo
fluffy.save(function (err, fluffy) {
  if (err) return console.error(err);
  console.log(fluffy);
});
// Lee todos los elementos del modelo
Kitten.find({}, function (err, data) {
  if (err) return console.error(err);
  console.log(data);
});


    // const estudiante = new Estudiante({
    //   documento: "1000",
    //   apellidos: "Cardozo Ramirez",
    //   nombres: "John Alexander",
    //   email: "john@gmail.com",
    //   semestre: 2,
    //   yaVoto: false,
    // });
    // estudiante.save(function (err, est) {
    //   if (err) return console.error(err);
    //   console.log(est);
    // });


    // Obtiene la lista de ganadores
    // db.getCollection('estudiantes').find({votos: {$gt: 0}}).sort({votos: -1})