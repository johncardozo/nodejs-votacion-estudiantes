const mongoose = require("mongoose");

// Define el modelo del estudiante
const EstudianteSchema = mongoose.Schema({
  documento: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  apellidos: {
    type: String,
    required: true,
  },
  nombres: {
    type: String,
    required: true,
  },
  semestre: {
    type: Number,
    required: true,
  },
  facultad: {
    id: Number,
    nombre: String,
  },
  yaVoto: Boolean,
  candidato: {
    id: String,
    documento: String,
    apellidos: String,
    nombres: String,
  },
  fechaVotacion: Date,
  esCandidato: Boolean,
  votos: Number,
});

// Exporta el modelo del Estudiante
module.exports = mongoose.model("Estudiante", EstudianteSchema);
