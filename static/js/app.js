document.addEventListener("DOMContentLoaded", () => {
  // Oculta el mensaje de error
  var mensajeError = document.querySelector("#seleccionar-error");
  mensajeError.style.display = "None";

  // Obtiene el botón votar
  var botonVotar = document.querySelector("#botonVotar");

  if (botonVotar) {
    // Evento que detecta el click en el botón Votar
    botonVotar.addEventListener("click", (event) => {
      // Evita el comportamiento por defecto del botón
      event.preventDefault();
      // Obtiene los radio del formulario
      var checkboxes = document.querySelectorAll(".check-candidato");
      // Verifica si hay algún checkbox seleccionado
      var algun = Array.prototype.slice.call(checkboxes).some((c) => c.checked);
      if (algun) {
        // Obtiene el formulario
        var formulario = document.querySelector("#formulario");
        // Envía el formulario
        formulario.submit();
      } else {
        mensajeError.style.display = "block";
      }
    });
  }
});
