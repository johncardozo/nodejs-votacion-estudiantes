document.addEventListener("DOMContentLoaded", () => {
  generarPie();
  generarStackBar();
});

function generarPie() {
  // Obtiene el canvas del pie
  let canvasPie = document.getElementById("votantesPie");

  // Obtiene los valores del pie
  let totalNoVotantes = canvasPie.getAttribute("data-no-votantes");
  let totalVotantes = canvasPie.getAttribute("data-votantes");

  // Obtiene el contexto
  let contextoPie = canvasPie.getContext("2d");

  // Crea la gráfica
  let votantesPie = new Chart(contextoPie, {
    type: "pie",
    data: {
      datasets: [
        {
          data: [totalVotantes, totalNoVotantes],
          backgroundColor: [
            "rgba(75, 192, 192, 0.2)",
            "rgba(255, 99, 132, 0.2)",
          ],
          borderColor: ["rgba(75, 192, 192, 1)", "rgba(255, 99, 132, 1)"],
          borderWidth: 1,
        },
      ],
      labels: ["votantes", "no votantes"],
    },
  });
}

function generarStackBar() {
  // Obtiene el canvas del stackbar
  let canvasStackedBar = document.getElementById("votacionesStackedBar");

  // Obtiene el contexto
  let contextoStackBar = canvasStackedBar.getContext("2d");

  // Obtiene los valores del stackbar
  let listaSemestresAttr = canvasStackedBar.getAttribute(
    "data-lista-semestres"
  );
  // Convierte la lista de String a JSON
  let listaSemestres = JSON.parse(listaSemestresAttr);
    
  // Crea los datasets para la gráfica
  var dataNoVotantes = [];
  var dataVotantes = [];
  var labelsSemestres = []
  var semestre = 1;
  listaSemestres.forEach((elemento) => {
    dataNoVotantes.push(elemento["totalNoVotantes"]);
    dataVotantes.push(elemento["totalVotantes"]);
    labelsSemestres.push(semestre++);
  });

  var barChartData = {
    labels: labelsSemestres,
    datasets: [
      {
        label: "No votantes",
        backgroundColor: "rgba(255, 99, 132, 0.5)",
        data: dataNoVotantes,
      },
      {
        label: "Votantes",
        backgroundColor: "rgba(75, 192, 192, 0.5)",
        data: dataVotantes,
      },
    ],
  };

  var votacionesStacked = new Chart(contextoStackBar, {
    type: "bar",
    data: barChartData,
    options: {
      title: {
        display: true,
        text: "Votaciones por semestre",
      },
      tooltips: {
        mode: "index",
        intersect: false,
      },
      responsive: true,
      scales: {
        xAxes: [
          {
            stacked: true,
          },
        ],
        yAxes: [
          {
            stacked: true,
          },
        ],
      },
    },
  });
}
