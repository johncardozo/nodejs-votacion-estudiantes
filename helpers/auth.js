const helpers = {};

helpers.isAuthenticated = (req, res, next) => {
  // Veririca si el usuario está autenticado
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect("/login");
};

module.exports = helpers;
