# Sistema de Votación para Representante Estudiantil

Aplicación desarrollada para permitir que los estudiantes de la Facultad de Ingeniería de Telecomunicaciones lleven a cabo la votación de representante estudiantil.

Los estudiantes se autentican con su email y documento de indentidad. Una vez ingresan se les muestran los candidatos a representante estudiantil del semestre al que pertenecen. Por ejemplo, si se autentica un estudiante de 2do. semestre le mostrará únicamente los candidatos de 2do. semestre.

La aplicación permite sólo votar una vez. Si el estudiante se autentica en la aplicación y ya ha votado anteriormente, se le mostrará el candidato por el cual votó y la fecha y hora en la que realizó el votoy no le permitirá votar de nuevo.

Esta aplicación está desarrollado con las siguiente tecnologías:

- Nodejs - [https://nodejs.org](https://nodejs.org/)
- Express - [https://expressjs.com](https://expressjs.com/)
- Mongodb - [https://www.mongodb.com](https://www.mongodb.com/)

## Instalación 
~~~bash
npm install
~~~

## Ejecución
~~~bash
npm start
~~~
